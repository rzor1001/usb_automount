#!/usr/bin/env python
#to automatically mount an NTFS partition requires the ntfs-3g package
#test

import os,sys
from time import sleep

os.system('rm -r /media/usb*')
actual_list = []
def refresh():
	disks_list = os.popen('lsblk -r -o name,maj:min,rm,size,ro,type,mountpoint,label').read()
	disks_list = disks_list.split('\n')
	disks = []
	for disk in disks_list:
		disk = disk.split(' ')
		upgrade = False
		if len(disk) > 6:
			if disk[7] == 'UPGRADE': 
				upgrade = True
		if len(disk) > 5 and disk[5]=='part' and not disk[0].startswith('mmcblk'): disks.append([disk[0], None, False, upgrade])
	return disks

def get_mount_point():
	for count in range(20):
		found = True
		to_check = '/media/usb' + str(count)
		if not os.path.isdir(to_check):
			return to_check
			break
			
def check_ntfs(partiton):
	info = os.popen('sudo blkid %s' % partiton).read()
	info = info.split(' ')
	if info[2] == 'TYPE="ntfs"': return True
	else: return False
		
while True:
	sleep(1)
	refresh_list = refresh()
	count0 = 0
	count1 = 0
	if len(refresh_list) > len(actual_list):
		for unmounted in refresh_list:
			for mounted in actual_list:
				if mounted[0] == unmounted[0]:
					refresh_list[count0][1] = actual_list[count1][1]
				count1 += 1
			count1 = 0
			count0 += 1
		count0 = 0
		for unmounted in refresh_list:
			if not unmounted[1]:
				mount_point = get_mount_point()	
				if check_ntfs('/dev/%s' % unmounted[0]):
					if refresh_list[count0][3] == True:
						os.system('sudo mkdir /media/UPGRADE')
						refresh_list[count0][1] = '/media/UPGRADE'
						os.system('sudo ntfs-3g /dev/%s /media/UPGRADE' % str(unmounted[0]))
					else:
						refresh_list[count0][1] = mount_point
						os.system('sudo mkdir ' + str(refresh_list[count0][1]))
						os.system('sudo ntfs-3g /dev/%s %s' % (str(unmounted[0]), refresh_list[count0][1]))
				else:
					if refresh_list[count0][3] == True:
						os.system('sudo mkdir /media/UPGRADE')
						refresh_list[count0][1] = '/media/UPGRADE'
						os.system('sudo mount /dev/%s /media/UPGRADE' % str(unmounted[0]))
					else:
						refresh_list[count0][1] = mount_point
						os.system('sudo mkdir ' + str(refresh_list[count0][1]))
						os.system('sudo mount /dev/%s %s' % (str(unmounted[0]), refresh_list[count0][1]))
			count0 += 1
		actual_list = refresh_list
			
	elif len(refresh_list) < len(actual_list):
		for mounted in actual_list:
			for unmounted in refresh_list:
				if mounted[0] == unmounted[0]:
					actual_list[count0][2] = True
			count0 += 1
			
		for mounted in actual_list:
			if not mounted[2]:
				os.system('sudo umount %s' % str(mounted[1]))
				os.system('sudo rm -r ' + str(mounted[1]))
			count1 += 1
		actual_list = refresh_list